﻿using HarmonyLib;
using System;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;
using TaleWorlds.Engine.Screens;
using TaleWorlds.InputSystem;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;

namespace PhateModShowRecruitsTooltip
{
    class SubModule : MBSubModuleBase
    {
        protected override void OnSubModuleLoad()
        {
            Harmony.DEBUG = true;
            Harmony harmony = new Harmony("com.goog.bannerlordmods.PhateMod_ShowRecruitsTooltip");
            harmony.PatchAll();
            base.OnSubModuleLoad();
        }

        protected override void OnBeforeInitialModuleScreenSetAsRoot()
        {
            InformationManager.DisplayMessage(new InformationMessage("Loaded PhateMod_ShowRecruitsTooltip.", Color.FromUint(4282569842U)));
        }
    }
}
